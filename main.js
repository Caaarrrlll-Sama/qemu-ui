const electron = require("electron");
const url = require("url");
const path = require("path");
const { app, BrowserWindow, Menu } = electron;
const os = require('os')
global.sharedObj = { cpuCores: os.cpus().length };

let mainWindow;

//listen for app to be ready
app.on("ready", function() {
  mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    }
  });
  mainWindow.loadFile("index.html");

  //build menu
  const mainMenu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(mainMenu);
});

app.on("window-all-closed", function() {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

//menu temp
const menuTemplate = [
  {
    label: "File",
    submenu: [
      {
        role: "reload"
      },
      {
        label: "Quit",
        //accelerator = shortcut keys
        accelerator: process.platform == "darwin" ? "Command+Q" : "Ctrl+Q",
        click() {
          app.quit();
        }
      }
    ]
  },
  //comment this block out when production version is pushed
  {
    label: "Window",
    submenu: [
      {
        label: "Toggle Dev Tools",
        accelerator: process.platform == "darwin" ? "Command+I" : "Ctrl+I",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        }
      }
    ]
  }
];

// if (process.platform == "darwin") {
//   menuTemplate.unshift({});
// }
